
package prod.sql.run.Manager;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import prod.sql.run.Objects.Landi;

public class ProdSqlRunManagerTest {

  private DateFormat format;
  private Date date;

  @Before
  public void buildUp() {
    format = new SimpleDateFormat("dd-MM-yyyy");
    date = new Date();
  }

  @Test
  public void sort_sortTwoLandis_LandisSortedCorrectly() {
    ProdSqlRunManager prodSqlRunManager = new ProdSqlRunManager();
    ArrayList<Landi> listToAdd = new ArrayList<Landi>();
    Landi landiToTest = new Landi("Test", "", "", "");
    Landi landiToTest2 = new Landi("ATest", "", "", "");
    listToAdd.add(landiToTest);
    listToAdd.add(landiToTest2);
    ObservableList<Landi> landiList = FXCollections.observableArrayList(listToAdd);
    assertThat(prodSqlRunManager.sortLandis(landiList).get(0).getName(), is("ATest"));
  }

  @Test
  public void format_formatTwoStatementWithNoSemicolonComments_getStatement() {
    ProdSqlRunManager manager = new ProdSqlRunManager();
    String sqlStatement = "Select * form bas$mba;select * from sal$cus;";
    manager.formatStatement(sqlStatement);
    assertThat(manager.getStatements().size(), is(2));
    assertThat(manager.getStatements().get(0), is("Select * form bas$mba"));
    assertThat(manager.getStatements().get(1), is("select * from sal$cus"));
  }

  @Test
  public void format_formatTwoStatementWithSemicolonComments_getStatement() {
    ProdSqlRunManager manager = new ProdSqlRunManager();
    String sqlStatement = "Select * form /*sample ; comment*/ bas$mba;select * from /*sample ; comment*/sal$cus;";
    manager.formatStatement(sqlStatement);
    assertThat(manager.getStatements().size(), is(2));
    assertThat(manager.getStatements().get(0), is("Select * form /*sample  comment*/ bas$mba"));
    assertThat(manager.getStatements().get(1), is("select * from /*sample  comment*/sal$cus"));
  }

  @Test
  public void format_formatTwoStatementWithSemicolonInString_getStatement() {
    ProdSqlRunManager manager = new ProdSqlRunManager();
    String sqlStatement = "Select * form bas$mba where mbaCode='200;';select * from sal$cus where cusIdent='name;';";
    manager.formatStatement(sqlStatement);
    assertThat(manager.getStatements().size(), is(2));
    assertThat(manager.getStatements().get(0), is("Select * form bas$mba where mbaCode='200;'"));
    assertThat(manager.getStatements().get(1), is("select * from sal$cus where cusIdent='name'"));
  }

}

/**
 * File Name: ConnectionFailedException.java
 * 
 * Copyright (c) 2016 BISON Schweiz AG, All Rights Reserved.
 */

package prod.sql.run.Exception;

import prod.sql.run.Objects.Landi;

public class ExecutionFailedException extends Exception {

  private static final long serialVersionUID = -4839192199482787399L;
  private Landi landi;

  public ExecutionFailedException(String message) {
    super(message);
  }

  public Landi getLandi() {
    return landi;
  }

  public void setLandi(Landi landi) {
    this.landi = landi;
  }

}

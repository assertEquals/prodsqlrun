/**
 * File Name: Landi.java
 * 
 * Copyright (c) 2016 BISON Schweiz AG, All Rights Reserved.
 */

package prod.sql.run.Objects;

/**
 * @author elias.broger
 */
public class Landi {

  private String name;
  private String connectionString;
  private String username;
  private String password;
  private String dataFrom;

  public Landi(String name, String connection, String username, String password) {
    setConnectionString(connection);
    setName(name);
    setUsername(username);
    this.password = password;
    this.dataFrom = null;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getConnectionString() {
    return connectionString;
  }

  public void setConnectionString(String connection) {
    this.connectionString = connection;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  /**
   * @return the dataFrom
   */
  public String getDataFrom() {
    return dataFrom;
  }

  /**
   * @param dataFrom the dataFrom to set
   */
  public void setDataFrom(String dataFrom) {
    this.dataFrom = dataFrom;
  }

}

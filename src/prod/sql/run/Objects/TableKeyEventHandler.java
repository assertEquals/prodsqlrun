/**
 * File Name: TableKeyEventHandler.java
 * 
 * Copyright (c) 2016 BISON Schweiz AG, All Rights Reserved.
 */

package prod.sql.run.Objects;

import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;

public class TableKeyEventHandler implements EventHandler<KeyEvent> {

  KeyCodeCombination keyCodeCombination = new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_ANY);

  @Override
  public void handle(KeyEvent event) {
    if (keyCodeCombination.match(event)) {
      if (event.getSource() instanceof TableView) {
        copySelectionToClipboard((TableView<?>)event.getSource());

        event.consume();
      }
    }

  }

  /**
   * @param source
   */
  private void copySelectionToClipboard(TableView<?> table) {
    StringBuilder clipBoardString = new StringBuilder();

    ObservableList<TablePosition> positionList = table.getSelectionModel().getSelectedCells();

    int prevRow = -1;

    for (TablePosition<?, ?> position : positionList) {

      int row = position.getRow();

      Object cell = position.getTableColumn().getCellData(row);

      // null-check: provide empty string for nulls
      if (cell == null) {
        cell = "";
      }

      // determine whether we advance in a row (tab) or a column
      // (newline).
      if (prevRow == row) {

        clipBoardString.append('\t');

      } else if (prevRow != -1) {

        clipBoardString.append('\n');

      }

      // create string from cell
      String text = cell.toString();

      // add new item to clipboard
      clipBoardString.append(text);

      // remember previous
      prevRow = row;
    }

    final ClipboardContent clipBoardContent = new ClipboardContent();
    clipBoardContent.putString(clipBoardString.toString());

    Clipboard.getSystemClipboard().setContent(clipBoardContent);

  }

}

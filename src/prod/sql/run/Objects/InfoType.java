/**
 * File Name: InfoType.java
 * 
 * Copyright (c) 2016 BISON Schweiz AG, All Rights Reserved.
 */

package prod.sql.run.Objects;

public enum InfoType {
  ERROR, WARNING, SUCCESS;
}

/**
 * File Name: ResultBorderPane.java
 * 
 * Copyright (c) 2016 BISON Schweiz AG, All Rights Reserved.
 */

package prod.sql.run.Objects;

import java.sql.ResultSet;
import java.sql.SQLException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.layout.BorderPane;

public class ResultBorderPane extends BorderPane {
  private Landi landi;
  private ObservableList<String> columns;
  private ObservableList<ObservableList<String>> rows;
  private ResultSet rs;

  public ResultBorderPane(Landi landi, ResultSet rs) {
    columns = FXCollections.observableArrayList();
    rows = FXCollections.observableArrayList();
    setLandi(landi);
    try {
      setRs(rs);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public ObservableList<String> getColumns() {
    return this.columns;
  }

  public ObservableList<ObservableList<String>> getRows() {
    return this.rows;
  }

  /**
   * @return the landi
   */
  public Landi getLandi() {
    return landi;
  }

  /**
   * @param landi the landi to set
   */
  public void setLandi(Landi landi) {
    this.landi = landi;
  }

  /**
   * @return the rs
   */
  public ResultSet getRs() {
    return rs;
  }

  /**
   * @param rs the rs to set
   * @throws SQLException
   */
  public void setRs(ResultSet rs) throws SQLException {
    for (int counter = 1; counter <= rs.getMetaData().getColumnCount(); counter++) {
      columns.add(rs.getMetaData().getColumnName(counter));
    }

    while (rs.next()) {
      ObservableList<String> row = FXCollections.observableArrayList();
      for (int counter = 1; counter <= rs.getMetaData().getColumnCount(); counter++) {
        row.add(rs.getString(counter));
      }
      rows.add(row);
    }
    this.rs = rs;
  }

}

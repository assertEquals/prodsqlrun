/**
 * File Name: LandiComperator.java
 * 
 * Copyright (c) 2016 BISON Schweiz AG, All Rights Reserved.
 */

package prod.sql.run.Objects;

import java.util.Comparator;

/**
 * @author elias.broger
 */
public class LandiComperator implements Comparator<Landi> {

  @Override
  public int compare(Landi ldi1, Landi ldi2) {
    return ldi1.getName().compareTo(ldi2.getName());
  }

}

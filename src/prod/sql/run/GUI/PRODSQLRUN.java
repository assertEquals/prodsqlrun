/**
 * File Name: PRODSQLRUN.java
 * 
 * Copyright (c) 2016 BISON Schweiz AG, All Rights Reserved.
 */

package prod.sql.run.GUI;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.function.Predicate;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import javafx.animation.PauseTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;
import prod.sql.run.DataSource.DataSource;
import prod.sql.run.Exception.ExecutionFailedException;
import prod.sql.run.Log.ProdSqlRunLogger;
import prod.sql.run.Manager.ProdSqlRunManager;
import prod.sql.run.Objects.Landi;
import prod.sql.run.Objects.toggleSwitchButton.ToggleSwitch;
import prodsqlrun.Messages;

/**
 * @author elias.broger
 */
public class PRODSQLRUN extends Application {

  private DataSource dataSource;
  private ProdSqlRunManager prodSqlRunManager;

  private ListView<Landi> listViewSelectedLandis;
  private ObservableList<Landi> selectedLandis;
  private ListProperty<Landi> listPropertySelected;

  private ListView<Landi> listViewDefaultLandis;
  private ObservableList<Landi> defaultLandis;
  private ListProperty<Landi> listPropertyDefault;

  private FilteredList<Landi> filteredDefaultLandiList;
  private FilteredList<Landi> filteredSelectedLandiList;

  private static final String INVALID_XML_MESSAGE = Messages.getString("INVALID_XML_MESSAGE");
  private static final String LOAD_SUCCESSFULL_MESSAGE = Messages.getString("LOAD_SUCCESSFULL_MESSAGE");
  private static final String STATEMENT_EXECUTE_SUCCESSFULL_MESSAGE = Messages.getString("STATEMENT_EXECUTE_SUCCESSFULL_MESSAGE");
  private static final String DEFAULT_LOAD_FAILED_MESSAGE = Messages.getString("DEFAULT_LOAD_FAILED_MESSAGE");
  private static final String NO_FILES_TO_LOAD_FOUND = Messages.getString("NO_FILES_TO_LOAD_FOUND");
  private static final String QUERY_FAILED = Messages.getString("QUERY_FAILED");
  private static final String DEFAULT_FILE_PATH = "K:/Allg/Lehrling/Lernende_2014/SQL-Run";
  private static final String DEFAULT_FILE_NAME = "SQL_Developer_Verbindungen.xml";
  private static final String SQL_EXPORT_ERROR = Messages.getString("sqlExportFailed");
  protected static final String SQL_EXPORT_SUCCESS = Messages.getString("sqlExportSuccess");
  private Label labelSelectAll;
  private Label labelSelectOne;
  private Label labelUnselectOne;
  private Label labelUnselectAll;
  private Label labelInfo;
  private Button buttonExecute;
  private VBox vBoxSelectionButtons;
  private Region region;
  private BorderPane panelWrapperCenter;
  private BorderPane panelWrapperCenterBottom;
  private BorderPane panelWrapperCenterTop;
  private BorderPane panelWrapperLeftTop;
  private BorderPane panelWrapperLeft;
  private BorderPane panelLandiInfo;
  private BorderPane loggerPanel;
  private SplitPane rootPane;
  private StackPane stackPane;
  private ProdSqlRunLogger logger;
  private WebView logWebArea;
  private Stage primaryStage;
  private BorderPane panelWrapperLeftLeft;
  private BorderPane panelWrapperLeftRight;
  private BorderPane popupPane;
  private Label labelPopupInfo;
  private ResultsGUI resultsGUI;
  private Label labelLastRunInfo;
  private Label labelLastRunIcon;
  private Label labelLog;
  private TextField textFieldFilterDefaultList;
  private TextField textFieldFilterSelectedList;
  private WebView webViewSQLStatement;
  private StackPane panelWrapperRight;
  private BorderPane panelWrapperLoggerTop;
private ToggleSwitch autocommit;

  public PRODSQLRUN() {

    loggerPanel = new BorderPane();
    panelLandiInfo = new BorderPane();
    panelWrapperCenterBottom = new BorderPane();
    panelWrapperCenterTop = new BorderPane();
    panelWrapperLeftTop = new BorderPane();
    panelWrapperLeft = new BorderPane();

    dataSource = new DataSource();
    logger = new ProdSqlRunLogger();
    logWebArea = new WebView();
    logWebArea.getStyleClass().add("shadow");

    listViewDefaultLandis = new ListView<Landi>();
    listViewDefaultLandis.getStyleClass().add("list-view");

    defaultLandis = FXCollections.observableArrayList();
    listPropertyDefault = new SimpleListProperty<Landi>();

    listViewSelectedLandis = new ListView<Landi>();
    listViewSelectedLandis.getStyleClass().add("list-view");

    selectedLandis = FXCollections.observableArrayList();
    listPropertySelected = new SimpleListProperty<Landi>();

    filteredDefaultLandiList = new FilteredList<Landi>(defaultLandis, s -> true);
    filteredSelectedLandiList = new FilteredList<Landi>(selectedLandis, s -> true);

    initPopup();
    bindListViews();
  }

  @Override
  public void start(Stage stageprimaryStage) throws Exception {
    this.primaryStage = stageprimaryStage;
    new PRODSQLRUN();
    resultsGUI = new ResultsGUI();
    prodSqlRunManager = new ProdSqlRunManager(this, resultsGUI);
    rootPane = new SplitPane();
    rootPane.setId("rootPane");
    Node divider = rootPane.lookup(".split-pane-divider");
    if (divider != null) {
      divider.setId("split-pane-divider");
    }
    panelLandiInfo.getStyleClass().add("panel");
    region = new Region();
    region.setId("popup-on");

    labelInfo = new Label();
    panelWrapperCenterBottom.setCenter(labelInfo);

    panelWrapperCenter = new BorderPane();
    panelWrapperCenter.getStyleClass().add("panel");
    webViewSQLStatement = new WebView();

    webViewSQLStatement.getStyleClass().add("shadow");

    webViewSQLStatement.getEngine().load(getClass().getResource("/gui/resources/codeMirror/CodeMirrorTextArea.html").toExternalForm());

    webViewSQLStatement.setOnKeyPressed(new EventHandler<KeyEvent>() {

      @Override
      public void handle(KeyEvent event) {

        if (event.getCode().equals(KeyCode.ENTER) && event.isControlDown()) {
          execute(getTextAreaSelectedContent());
        }

        if (event.getCode().equals(KeyCode.F5)) {
          execute(getTextAreaContent());
        }

        if (event.getCode().equals(KeyCode.S) && event.isControlDown()) {
          FileChooser fileChooser = new FileChooser();
          fileChooser.setTitle(Messages.getString("saveSQL"));
          fileChooser.getExtensionFilters().addAll(new ExtensionFilter("SQL scripts", "*.sql"), new ExtensionFilter("TextFile", "*.txt"));
          File saveFile = fileChooser.showSaveDialog(primaryStage);
          try {
            if (saveFile != null) {
              prodSqlRunManager.saveSQLFile(getTextAreaContent(), saveFile);
              showSuccess(SQL_EXPORT_SUCCESS, 5);
            }
          } catch (IOException e) {
            e.printStackTrace();
            showError(SQL_EXPORT_ERROR, 5);
          }
        }
      }

    });
    panelWrapperCenter.setCenter(webViewSQLStatement);

    Label labelSQLStatement = new Label(Messages.getString("labelSQLStatement"));
    labelSQLStatement.getStyleClass().add("label-bold");

    panelWrapperCenterTop.getStyleClass().add("panelWrapper");
    panelWrapperCenterTop.setCenter(labelSQLStatement);
    panelWrapperCenterTop.setPrefHeight(35);
    HBox hBoxAutoCommit = new HBox();
    autocommit = new ToggleSwitch();
    Label labelAutocommit = new Label("Auto commit");
    labelAutocommit.setOnMouseClicked(new EventHandler<Event>() {

		@Override
		public void handle(Event arg0) {
			PRODSQLRUN.this.autocommit.fire();
		}
	});
    
    hBoxAutoCommit.getChildren().addAll(this.autocommit, labelAutocommit);
    hBoxAutoCommit.setSpacing(10);
    hBoxAutoCommit.getStyleClass().add("panelWrapper");
    hBoxAutoCommit.setId("hBoxAutoCommit");
    panelWrapperCenterTop.setRight(hBoxAutoCommit);
    panelWrapperCenter.setTop(panelWrapperCenterTop);

    HBox hBoxLeftTopLeft = new HBox();
    labelLastRunInfo = new Label(Messages.getString("labelLastRunInfo"));
    labelLastRunInfo.getStyleClass().add("label-bold");
    labelLastRunIcon = new Label();
    labelLastRunIcon.setId("run-neutral");
    labelLog = new Label(Messages.getString("labelLog"));
    labelLog.setOnMouseClicked(new EventHandler<MouseEvent>() {

      @Override
      public void handle(MouseEvent event) {
        if (isDisplayed(loggerPanel)) {
          closeLogView();
        } else {
          openLogView();
        }
      }

    });
    labelLog.setId("label-link");
    hBoxLeftTopLeft.getChildren().addAll(labelLastRunInfo, labelLastRunIcon, labelLog);
    hBoxLeftTopLeft.setId("vBoxLTL");

    panelWrapperLeftTop.setLeft(hBoxLeftTopLeft);
    panelWrapperLeftTop.getStyleClass().add("panelWrapper");
    panelWrapperLeftTop.setPrefHeight(35);
    panelWrapperLeft.setTop(panelWrapperLeftTop);

    Label labelCustomData = new Label();
    labelCustomData.setId("label-customdata");
    panelWrapperLeftTop.setRight(labelCustomData);
    Tooltip tooltipInfoFile = new Tooltip(Messages.getString("tooltipInfoFile"));
    Tooltip.install(labelCustomData, tooltipInfoFile);
    labelCustomData.setOnMouseClicked(new EventHandler<MouseEvent>() {

      @Override
      public void handle(MouseEvent event) {
        loadNewFile();
      }

    });

    buttonExecute = new Button(Messages.getString("buttonExecute"));
    buttonExecute.setId("buttonExecute");
    buttonExecute.setOnMouseClicked(new EventHandler<MouseEvent>() {

      @Override
      public void handle(MouseEvent event) {
        execute(getTextAreaSelectedContent());
      }

    });

    panelWrapperCenterBottom.setRight(buttonExecute);
    panelWrapperCenter.setBottom(panelWrapperCenterBottom);

    listViewDefaultLandis.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

    listViewDefaultLandis.setOnKeyPressed(new EventHandler<KeyEvent>() {

      @Override
      public void handle(KeyEvent event) {
        if (event.getCode().equals(KeyCode.F) && event.isControlDown()) {
          textFieldFilterDefaultList.requestFocus();
        }
      }
    });

    panelWrapperLeftLeft = new BorderPane();
    panelWrapperLeftLeft.setCenter(listViewDefaultLandis);

    initializeVBox();

    listViewSelectedLandis.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    listViewSelectedLandis.setOnKeyPressed(new EventHandler<KeyEvent>() {

      @Override
      public void handle(KeyEvent event) {
        if (event.getCode().equals(KeyCode.F) && event.isControlDown()) {
          textFieldFilterSelectedList.requestFocus();
        }
      }
    });

    panelWrapperLeftRight = new BorderPane();
    panelWrapperLeftRight.getStyleClass().add("panelWrapperList");
    panelWrapperLeftRight.setCenter(listViewSelectedLandis);

    HBox hBoxLists = new HBox();
    hBoxLists.getChildren().addAll(panelWrapperLeftLeft, vBoxSelectionButtons, panelWrapperLeftRight);
    HBox.setHgrow(panelWrapperLeftLeft, Priority.ALWAYS);
    HBox.setHgrow(vBoxSelectionButtons, Priority.NEVER);
    HBox.setHgrow(panelWrapperLeftRight, Priority.ALWAYS);

    panelWrapperLeft.setCenter(hBoxLists);

    loadListViewDefault();

    addSearchField();

    createCellFactoryDefaultList(listViewDefaultLandis);
    createCellFactorySelectedList(listViewSelectedLandis);

    Platform.runLater(new Runnable() {

      @Override
      public void run() {
        webViewSQLStatement.requestFocus();
      }
    });

    panelWrapperRight = new StackPane();
    panelWrapperRight.getChildren().addAll(loggerPanel, panelLandiInfo, panelWrapperCenter);

    rootPane.getItems().addAll(panelWrapperLeft, panelWrapperRight);
    rootPane.setDividerPosition(0, 0.45f);
    stackPane = new StackPane();
    stackPane.getChildren().addAll(rootPane);
    Scene scene = new Scene(stackPane, 1150, 600);
    scene.getStylesheets().add(getClass().getResource("/gui/resources/PRODSQLRUN.css").toExternalForm());
    primaryStage.setScene(scene);
    primaryStage.setTitle("Prod Sql Run");
    primaryStage.getIcons().add(new Image(this.getClass().getResourceAsStream("/gui/resources/icon.png")));
    primaryStage.show();
  }

  /**
   * run statement in own thread
   * @param statement sql statement
   */
  private void execute(String statement) {
    if (selectedLandis.isEmpty() || statement.isEmpty()) {
      return;
    }
    Task<Void> task;
    String stmt = statement;
    task = runStatement(stmt);
    task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

      @Override
      public void handle(WorkerStateEvent workerStateEvent) {
        closePopup();
      }
    });
  }

  private Task<Void> runStatement(String statement) {
    showPopup();
    Task<Void> task = new Task<Void>() {

      @Override
      protected Void call() throws Exception {
        ArrayList<Landi> executeLandi = new ArrayList<Landi>();
        executeLandi.addAll(selectedLandis);
        ArrayList<ExecutionFailedException> errorList = prodSqlRunManager.execute(statement.trim(), executeLandi, autocommit.isSelected());
        Platform.runLater(new Runnable() {

          @Override
          public void run() {
            if (errorList.isEmpty()) {
              showSuccess(STATEMENT_EXECUTE_SUCCESSFULL_MESSAGE, 5);
              labelLastRunIcon.setId("run-green");
            } else {
              showError(QUERY_FAILED, 5);
              labelLastRunIcon.setId("run-red");
            }
            logger.log(executeLandi, errorList, statement.trim());
          }
        });
        return null;
      }
    };
    new Thread(task).start();
    return task;
  }

  public void showPopup() {
    if (stackPane.getChildren().contains(popupPane)) {
      return;
    }
    stackPane.getChildren().addAll(region, popupPane);
  }

  public void closePopup() {
    stackPane.getChildren().removeAll(region, popupPane);
  }

  public void updatePopup(String text) {
    labelPopupInfo.setText(text);
  }

  /**
   * Initialize popup which is been shown when sql is running
   */

  public void initPopup() {
    labelPopupInfo = new Label();
    labelPopupInfo.setId("label-popup-info");
    popupPane = new BorderPane();
    ProgressIndicator progressIndicator = new ProgressIndicator();
    progressIndicator.setMaxSize(100, 100);
    progressIndicator.setMinSize(100, 100);
    VBox vBox = new VBox();
    vBox.setAlignment(Pos.CENTER);
    vBox.setSpacing(5);
    vBox.getChildren().addAll(progressIndicator, labelPopupInfo);
    popupPane.setCenter(vBox);
  }

  private void addSearchField() {
    textFieldFilterDefaultList = new TextField();
    textFieldFilterDefaultList.setPromptText(Messages.getString("textFieldFilterList"));
    textFieldFilterDefaultList.focusedProperty().addListener(new ChangeListener<Boolean>() {

      @Override
      public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
        Platform.runLater(new Runnable() {

          @Override
          public void run() {
            if (newValue.booleanValue()) {
              textFieldFilterDefaultList.selectAll();
            }
          }
        });
      }

    });

    textFieldFilterDefaultList.textProperty().addListener(new ChangeListener<String>() {

      @Override
      public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        filteredDefaultLandiList.setPredicate(new Predicate<Landi>() {

          @Override
          public boolean test(Landi landi) {
            if (newValue == null || newValue.isEmpty()) {
              return true;
            }

            String lowerCaseFilter = newValue.toLowerCase();

            if (landi.getName().toLowerCase().contains(lowerCaseFilter)) {
              return true;
            }
            if (landi.getDataFrom() != null) {
              if (landi.getDataFrom().toLowerCase().contains(lowerCaseFilter)) {
                return true;
              }
            }
            return false;
          }
        });
      }
    });
    panelWrapperLeftLeft.setTop(textFieldFilterDefaultList);
    listViewDefaultLandis.setItems(filteredDefaultLandiList);
    textFieldFilterSelectedList = new TextField();
    textFieldFilterSelectedList.setPromptText(Messages.getString("textFieldFilterList"));
    textFieldFilterSelectedList.focusedProperty().addListener(new ChangeListener<Boolean>() {

      @Override
      public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
        Platform.runLater(new Runnable() {

          @Override
          public void run() {
            if (newValue.booleanValue()) {
              textFieldFilterSelectedList.selectAll();
            }
          }
        });
      }

    });

    textFieldFilterSelectedList.textProperty().addListener(new ChangeListener<String>() {

      @Override
      public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        filteredSelectedLandiList.setPredicate(new Predicate<Landi>() {

          @Override
          public boolean test(Landi landi) {
            if (newValue == null || newValue.isEmpty()) {
              return true;
            }

            String lowerCaseFilter = newValue.toLowerCase();

            if (landi.getName().toLowerCase().contains(lowerCaseFilter)) {
              return true;
            }
            if (landi.getDataFrom() != null) {
              if (landi.getDataFrom().toLowerCase().contains(lowerCaseFilter)) {
                return true;
              }
            }
            return false;
          }
        });
      }
    });
    panelWrapperLeftRight.setTop(textFieldFilterSelectedList);
    listViewSelectedLandis.setItems(filteredSelectedLandiList);
  }

  private void createCellFactoryDefaultList(ListView<Landi> rawListView) {
    rawListView.setCellFactory(new Callback<ListView<Landi>, ListCell<Landi>>() {

      @Override
      public ListCell<Landi> call(ListView<Landi> param) {
        ListCell<Landi> cell = new ListCell<Landi>() {

          @Override
          protected void updateItem(Landi landi, boolean empty) {
            super.updateItem(landi, empty);
            if (landi == null || empty) {
              setText(null);
            } else {
              setText(landi.getName());
              ContextMenu contextMenuDefaultCell = new ContextMenu();
              MenuItem info = new MenuItem(Messages.getString("info"));
              info.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {
                  displayLandiInfo(landi);
                }

              });
              contextMenuDefaultCell.getItems().add(info);
              setContextMenu(contextMenuDefaultCell);
              setOnMouseClicked(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {
                  if (event.getButton().equals(MouseButton.PRIMARY)) {
                    if (event.getClickCount() == 2) {
                      selectSingle();
                    }
                  }
                }
              });
            }
          }
        };
        return cell;
      }
    });
  }

  private void createCellFactorySelectedList(ListView<Landi> rawListView) {
    rawListView.setCellFactory(new Callback<ListView<Landi>, ListCell<Landi>>() {

      @Override
      public ListCell<Landi> call(ListView<Landi> param) {
        ListCell<Landi> cell = new ListCell<Landi>() {

          @Override
          protected void updateItem(Landi landi, boolean empty) {
            super.updateItem(landi, empty);
            if (landi == null || empty) {
              setText(null);
            } else {
              setText(landi.getName());
              ContextMenu contextMenuDefaultCell = new ContextMenu();
              MenuItem info = new MenuItem(Messages.getString("info"));
              info.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {
                  displayLandiInfo(landi);
                }

              });
              contextMenuDefaultCell.getItems().add(info);
              setContextMenu(contextMenuDefaultCell);
              setOnMouseClicked(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {
                  if (event.getButton().equals(MouseButton.PRIMARY)) {
                    if (event.getClickCount() == 2) {
                      unselectSingle();
                    }
                  }
                }
              });
            }
          }
        };
        return cell;
      }
    });
  }

  private void displayLandiInfo(Landi landi) {
    VBox vBoxLandiInfos = new VBox();
    vBoxLandiInfos.setId("vBoxLandiInfos");
    vBoxLandiInfos.getChildren().add(createInfoPane(Messages.getString("landiInfoName"), landi.getName()));
    vBoxLandiInfos.getChildren().add(createInfoPane(Messages.getString("landiInfoCon"), landi.getConnectionString()));
    vBoxLandiInfos.getChildren().add(createInfoPane(Messages.getString("landiInfoUsr"), landi.getUsername()));
    String dataBehind = landi.getDataFrom();
    if (dataBehind != null && !landi.getName().contains("PROD")) {
      vBoxLandiInfos.getChildren().add(createInfoPane(Messages.getString("landiInfoData"), dataBehind));
    }
    vBoxLandiInfos.getStyleClass().add("shadow");

    panelLandiInfo.setCenter(vBoxLandiInfos);
    BorderPane panelWrapperLandiInfoTop = new BorderPane();
    Label labelClose = new Label();
    labelClose.setOnMouseClicked(new EventHandler<MouseEvent>() {

      @Override
      public void handle(MouseEvent event) {
        closeLandiInfo();
      }

    });
    labelClose.getStyleClass().add("label-close");
    panelWrapperLandiInfoTop.setRight(labelClose);
    panelLandiInfo.setTop(panelWrapperLandiInfoTop);

    openLandiInfo();
  }

  private void openLogView() {
    loggerPanel.getStyleClass().add("panel");

    loggerPanel.setOnKeyPressed(new EventHandler<KeyEvent>() {

      @Override
      public void handle(KeyEvent event) {
        if (event.getCode().equals(KeyCode.S) && event.isControlDown()) {
          storeLog();
        }
      }
    });

    logWebArea.getEngine().loadContent(logger.getLog());
    Label labelCloseLog = new Label();
    labelCloseLog.getStyleClass().add("label-close");
    labelCloseLog.setOnMouseClicked(new EventHandler<MouseEvent>() {

      @Override
      public void handle(MouseEvent event) {
        closeLogView();
      }
    });

    panelWrapperLoggerTop = new BorderPane();
    Label labelStoreLog = new Label();
    labelStoreLog.setId("label-storelog");
    labelStoreLog.setOnMouseClicked(new EventHandler<MouseEvent>() {

      @Override
      public void handle(MouseEvent event) {
        storeLog();
      }
    });

    panelWrapperLoggerTop.setLeft(labelStoreLog);
    panelWrapperLoggerTop.setRight(labelCloseLog);
    loggerPanel.setCenter(logWebArea);
    loggerPanel.setTop(panelWrapperLoggerTop);

    displayPanel(loggerPanel);
    loggerPanel.requestFocus();
  }

  /**
   * @param panel
   */
  private void displayPanel(BorderPane panelToDisplay) {
    panelWrapperRight.getChildren().get(panelWrapperRight.getChildren().indexOf(panelToDisplay)).toFront();
    TranslateTransition translateTransition = new TranslateTransition(Duration.seconds(0.5), panelToDisplay);
    translateTransition.setFromX(panelWrapperRight.getWidth());
    translateTransition.setToX(0);
    translateTransition.play();
  }

  private void hidePanel(BorderPane panelToHide) {
    TranslateTransition translateTransition = new TranslateTransition(Duration.seconds(0.5), panelToHide);
    translateTransition.setFromX(0);
    translateTransition.setToX(panelWrapperRight.getWidth());
    translateTransition.play();
    translateTransition.setOnFinished(new EventHandler<ActionEvent>() {

      @Override
      public void handle(ActionEvent event) {
        panelWrapperRight.getChildren().get(panelWrapperRight.getChildren().indexOf(panelToHide)).toBack();
        panelWrapperRight.getChildren().get(panelWrapperRight.getChildren().indexOf(panelToHide)).toBack();
      }
    });
  }

  private boolean isDisplayed(BorderPane paneToCheck) {
    if (panelWrapperRight.getChildren().indexOf(paneToCheck) == 2) {
      return true;
    }
    return false;
  }

  private void storeLog() {
    DirectoryChooser dirChooser = new DirectoryChooser();
    dirChooser.setTitle(Messages.getString("dirChooserLog"));
    File dir = dirChooser.showDialog(primaryStage);
    try {
      if (dir != null) {
        logger.storeLog(dir.getPath());
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void closeLogView() {
    hidePanel(loggerPanel);
  }

  /**
   * close infopane of landis
   */

  private void closeLandiInfo() {
    hidePanel(panelLandiInfo);
  }

  /**
   * open infopane of landis
   */

  private void openLandiInfo() {
    if (!isDisplayed(panelLandiInfo)) {
      displayPanel(panelLandiInfo);
    }
  }

  private VBox createInfoPane(String description, String data) {
    Label labelDesc = new Label(description);
    labelDesc.getStyleClass().add("label-bold");
    Label labelData = new Label(data.trim());
    labelData.getStyleClass().add("label-Normal");
    VBox vBox = new VBox();
    vBox.getChildren().addAll(labelDesc, labelData);
    return vBox;
  }

  private void initializeVBox() {
    vBoxSelectionButtons = new VBox();
    vBoxSelectionButtons.setId("vBox-selectionButtons");

    labelSelectAll = new Label();
    labelSelectAll.setId("labelSelectAll");
    labelSelectAll.setMinSize(40, 40);
    labelSelectAll.setOnMouseClicked(new EventHandler<MouseEvent>() {

      @Override
      public void handle(MouseEvent event) {
        selectAll();
      }

      /**
       * Select all systems in default list
       */
      private void selectAll() {
        selectedLandis.addAll(filteredDefaultLandiList);
        defaultLandis.removeAll(filteredDefaultLandiList);
        listViewDefaultLandis.setItems(filteredDefaultLandiList);
        listViewSelectedLandis.setItems(filteredSelectedLandiList);
      }
    });

    labelSelectOne = new Label();
    labelSelectOne.setId("labelSelectOne");
    labelSelectOne.setMinSize(40, 40);
    labelSelectOne.setOnMouseClicked(new EventHandler<MouseEvent>() {

      @Override
      public void handle(MouseEvent event) {
        selectMarked();
      }

    });

    labelUnselectOne = new Label();
    labelUnselectOne.setId("labelUnselectOne");
    labelUnselectOne.setMinSize(40, 40);
    labelUnselectOne.setOnMouseClicked(new EventHandler<MouseEvent>() {

      @Override
      public void handle(MouseEvent event) {
        unselectMarked();
      }

    });

    labelUnselectAll = new Label();
    labelUnselectAll.setId("labelUnselectAll");
    labelUnselectAll.setMinSize(40, 40);
    labelUnselectAll.setOnMouseClicked(new EventHandler<MouseEvent>() {

      @Override
      public void handle(MouseEvent event) {
        unselectAll();
      }

      /**
       * Unselect all systems in selected list
       */
      private void unselectAll() {
        addLandisToDefaultLandiList(filteredSelectedLandiList);
        selectedLandis.removeAll(filteredSelectedLandiList);
        listViewDefaultLandis.setItems(filteredDefaultLandiList);
        listViewSelectedLandis.setItems(filteredSelectedLandiList);
      }
    });
    vBoxSelectionButtons.getChildren().addAll(labelSelectAll, labelSelectOne, labelUnselectOne, labelUnselectAll);
  }

  /**
   * Unselect the current selected system in selected list
   */
  private void unselectMarked() {
    ObservableList<Landi> selectedItems = listViewSelectedLandis.getSelectionModel().getSelectedItems();
    addLandisToDefaultLandiList(selectedItems);
    selectedLandis.removeAll(selectedItems);
    listViewDefaultLandis.setItems(filteredDefaultLandiList);
    listViewSelectedLandis.setItems(filteredSelectedLandiList);
  }

  private void unselectSingle() {
    Landi selectedItem = listViewSelectedLandis.getSelectionModel().getSelectedItem();
    defaultLandis.add(selectedItem);
    prodSqlRunManager.sortLandis(defaultLandis);
    selectedLandis.remove(selectedItem);
    listViewDefaultLandis.setItems(filteredDefaultLandiList);
    listViewSelectedLandis.setItems(filteredSelectedLandiList);
  }

  /**
   * Select the current selected system in default list
   */
  private void selectMarked() {
    ObservableList<Landi> selectedItems = listViewDefaultLandis.getSelectionModel().getSelectedItems();
    addLandisToSelectedLandiList(selectedItems);
    defaultLandis.removeAll(selectedItems);
    listViewDefaultLandis.setItems(filteredDefaultLandiList);
    listViewSelectedLandis.setItems(filteredSelectedLandiList);
  }

  private void selectSingle() {
    Landi selectedItem = listViewDefaultLandis.getSelectionModel().getSelectedItem();
    selectedLandis.add(selectedItem);
    prodSqlRunManager.sortLandis(selectedLandis);
    defaultLandis.remove(selectedItem);
    listViewDefaultLandis.setItems(filteredDefaultLandiList);
    listViewSelectedLandis.setItems(filteredSelectedLandiList);
  }

  private void bindListViews() {
    listPropertyDefault.set(defaultLandis);
    listViewDefaultLandis.itemsProperty().bindBidirectional(listPropertyDefault);
    listPropertySelected.set(selectedLandis);
    listViewSelectedLandis.itemsProperty().bindBidirectional(listPropertySelected);
  }

  /**
   * open file chooser for loading xml
   */
  public void loadNewFile() {
    FileChooser fileChooser = new FileChooser();
    ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
    fileChooser.getExtensionFilters().add(extensionFilter);
    fileChooser.setTitle(Messages.getString("fileChooserXML"));
    fileChooser.setInitialDirectory(new File(dataSource.getLastLoadDataPath()));
    File dir = fileChooser.showOpenDialog(primaryStage);
    if (dir != null) {
      ObservableList<Landi> insert;
      try {
        insert = FXCollections.observableArrayList(dataSource.getLandis(dir.getParentFile().toString(), dir.getName()));
        clearLandiLists();
        addLandisToDefaultLandiList(insert);
        showSuccess(LOAD_SUCCESSFULL_MESSAGE, 5);
      } catch (ParserConfigurationException | SAXException | IOException e) {
        showError(INVALID_XML_MESSAGE, 5);
      }
    } else {
      showError(NO_FILES_TO_LOAD_FOUND);
    }
  }

  /**
   * Displays a custom succeed message for a custom duration
   * 
   * @param message message on gui
   * @param duration Time in seconds that the message should be displayed
   */
  private void showSuccess(String message, int duration) {
    labelInfo.setText(message);
    labelInfo.setId("label-success");
    panelWrapperCenterBottom.setId("panel-success");
    PauseTransition showSuccess = new PauseTransition(Duration.seconds(duration));
    showSuccess.setOnFinished(new EventHandler<ActionEvent>() {

      @Override
      public void handle(ActionEvent event) {
        panelWrapperCenterBottom.setId("");
        labelInfo.setText("");
        labelInfo.setId("");
      }
    });
    showSuccess.play();
  }

  /**
   * Displays a custom error message for a custom duration
   * 
   * @param message message on gui
   * @param duration Time in seconds that the message should be displayed
   */
  private void showError(String message, int duration) {
    labelInfo.setText(message);
    labelInfo.setId("label-error");
    panelWrapperCenterBottom.setId("panel-error");
    PauseTransition showError = new PauseTransition(Duration.seconds(5));
    showError.setOnFinished(new EventHandler<ActionEvent>() {

      @Override
      public void handle(ActionEvent event) {
        labelInfo.setText("");
        labelInfo.setId("");
        panelWrapperCenterBottom.setId("");
      }
    });
    showError.play();
  }

  /**
   * Displays a custom error message
   * @param message message message on gui
   */

  private void showError(String message) {
    labelInfo.setText(message);
    labelInfo.setId("label-error");
    panelWrapperCenterBottom.setId("panel-error");
  }

  /**
   * Load all systems from default file path to defaultListView
   */
  private void loadListViewDefault() {
    if (new File(DEFAULT_FILE_PATH + "/" + DEFAULT_FILE_NAME).exists()) {
      ObservableList<Landi> insert;
      try {
        insert = FXCollections.observableArrayList(dataSource.getLandis(DEFAULT_FILE_PATH, DEFAULT_FILE_NAME));
        addLandisToDefaultLandiList(insert);
      } catch (ParserConfigurationException | SAXException | IOException e) {
        e.printStackTrace();
        showError(DEFAULT_LOAD_FAILED_MESSAGE, 5);
      }
    } else {
      loadNewFile();
    }
  }

  public String getTextAreaContent() {
    return (String)webViewSQLStatement.getEngine().executeScript("editor.getValue();");
  }

  public String getTextAreaSelectedContent() {
    String statement = (String)webViewSQLStatement.getEngine().executeScript("editor.getSelection();");
    return statement;
  }

  public void clearLandiLists() {
    defaultLandis.clear();
    selectedLandis.clear();
  }

  public void addLandisToDefaultLandiList(ObservableList<Landi> landiList) {
    defaultLandis.addAll(landiList);
    prodSqlRunManager.sortLandis(defaultLandis);
  }

  public ObservableList<Landi> getlistViewDefaultLandiContent() {
    return listViewDefaultLandis.getItems();
  }

  public void addLandisToSelectedLandiList(ObservableList<Landi> landiList) {
    selectedLandis.addAll(landiList);
    prodSqlRunManager.sortLandis(selectedLandis);
  }

  public ObservableList<Landi> getlistViewSelectedLandiContent() {
    return listViewSelectedLandis.getItems();
  }

  public static void main(String[] args) {
    launch(args);
  }

}

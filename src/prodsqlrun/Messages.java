/**
 * File Name: Messages.java
 * 
 * Copyright (c) 2016 BISON Schweiz AG, All Rights Reserved.
 */

package prodsqlrun;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import com.sun.media.jfxmedia.logging.Logger;

public class Messages {
  private static final String BUNDLE_NAME = "messages"; //$NON-NLS-1$

  private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

  private Messages() {
  }

  public static String getString(String key) {
    try {
      return RESOURCE_BUNDLE.getString(key);
    } catch (MissingResourceException e) {
      Logger.logMsg(0, e.getMessage());
      return "";
    }
  }
}

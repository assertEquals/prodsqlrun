
package prod.sql.run.DB;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import prod.sql.run.Exception.ExecutionFailedException;
import prod.sql.run.Objects.Landi;

public class DBConnectionTest {
  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  @Test
  public void connection_LandiWithConnectionString_getValidConnection() throws ExecutionFailedException, SQLException {
    DBConnection dbConnection = new DBConnection();
    Landi landiToTest = new Landi("Test_EV1", "jdbc:oracle:thin:@fenacosrv42081.main.corp.fenaco.com:1523/ezil2", "educv1ddl", "educv1ddl");
    Connection conn = dbConnection.getConnection(landiToTest);
    assertThat(conn.isValid(5), is(true));
  }

  @Test
  public void select_InvalidStatement_getSelectFailedException() throws ExecutionFailedException {
    DBConnection conn = new DBConnection();
    Landi landiToTest = new Landi("Test_EV1", "jdbc:oracle:thin:@fenacosrv42081.main.corp.fenaco.com:1523/ezil2", "educv1ddl", "educv1ddl");
    expectedException.expect(ExecutionFailedException.class);
    conn.execute("asdfasdf", landiToTest, true);
  }

  @Test
  public void update_InvalidStatement_getDMLFailedException() throws ExecutionFailedException {
    DBConnection conn = new DBConnection();
    Landi landiToTest = new Landi("Test_EV1", "jdbc:oracle:thin:@fenacosrv42081.main.corp.fenaco.com:1523/ezil2", "educv1ddl", "educv1ddl");
    expectedException.expect(ExecutionFailedException.class);
    conn.execute("ABCDEFG", landiToTest, true);
  }

  @Test
  public void getConnection_InvalidLandi_getExecutionFailedException() throws ExecutionFailedException {
    DBConnection conn = new DBConnection();
    Landi landiToTest = new Landi("Test_EV1", "jdbc:oracle:thin:@invalid.invalid.ch:1523/ivalid", "invalid", "invalid");
    expectedException.expect(ExecutionFailedException.class);
    conn.execute("ABCDEFG", landiToTest, true);
  }
}


package prod.sql.run.DataSource;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

public class DataSourceTest {

  private File file;

  @Before
  public void initialize() throws IOException {
    file = new File(System.getProperty("java.io.tmpdir") + "\\SQL-Run-Test.xml");
    file.createNewFile();
  }

  @Test
  public void load_loadEmptyXMLToArrayList_getEmptyArrayList() throws ParserConfigurationException, SAXException, IOException {
    FileWriter writer = new FileWriter(file);
    writer.write("<?xml version = '1.0' encoding = 'UTF-8'?><References xmlns=\"http://xmlns.oracle.com/adf/jndi\"></References>");
    writer.close();

    DataSource dataSource = new DataSource();
    assertTrue(dataSource.getLandis(System.getProperty("java.io.tmpdir"), "SQL-Run-Test.xml").isEmpty());
  }

  @Test
  public void load_loadXMLToArrayList_getArrayListWithData() throws ParserConfigurationException, SAXException, IOException {
    FileWriter writer = new FileWriter(file);
    Scanner scanner = new Scanner(new File(System.getProperty("user.dir") + "\\Test\\prod\\sql\\run\\DataSource\\SQL-Run-Test.xml"));
    String xmlText = "";
    String lineSeparator = System.getProperty("line.separator");
    while (scanner.hasNext()) {
      xmlText += scanner.nextLine() + lineSeparator;
    }
    scanner.close();
    writer.write(xmlText);
    writer.close();
    DataSource dataSource = new DataSource();
    assertThat(dataSource.getLandis(System.getProperty("java.io.tmpdir"), "SQL-Run-Test.xml").get(0).getUsername().trim(), is("test"));
  }

  @After
  public void cleanUp() {
    new File(System.getProperty("java.io.tmpdir") + "\\SQL-Run-Test.xml").delete();
  }

}

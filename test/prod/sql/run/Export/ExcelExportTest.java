
package prod.sql.run.Export;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import org.junit.Test;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import prod.sql.run.Objects.ExcelExportInfo;
import prod.sql.run.Objects.Landi;

public class ExcelExportTest {
  @Test
  public void export_getFileInDir() throws SQLException, IOException {
    ExcelExport excelExport = new ExcelExport("Select-Statement");
    ObservableList<String> col = FXCollections.observableArrayList();
    ObservableList<ObservableList<String>> rows = FXCollections.observableArrayList();
    excelExport.addToExport(col, rows, new Landi("TestLandi", "tst", "", ""), ExcelExportInfo.singleTab);
    excelExport.exportToExcel(System.getProperty("java.io.tmpdir") + "prodsqlrun-test.xls");
    File file = new File(System.getProperty("java.io.tmpdir") + "prodsqlrun-test.xls");
    assertTrue(file.exists());
    file.delete();
  }
}

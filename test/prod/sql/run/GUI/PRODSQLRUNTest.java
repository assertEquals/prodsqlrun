
package prod.sql.run.GUI;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.JFXPanel;
import prod.sql.run.Objects.Landi;

public class PRODSQLRUNTest {

  @Test
  public void bind_insertLandi_ListViewDefaultLandisConatinsLandi() {
    new JFXPanel();
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        PRODSQLRUN prodsqlrun = new PRODSQLRUN();
        ArrayList<Landi> listToAdd = new ArrayList<Landi>();
        Landi landiToTest = new Landi("Test", "", "", "");
        listToAdd.add(landiToTest);
        ObservableList<Landi> landiList = FXCollections.observableArrayList(listToAdd);
        prodsqlrun.addLandisToDefaultLandiList(landiList);
        assertThat(prodsqlrun.getlistViewDefaultLandiContent(), hasItem(landiToTest));
      }
    });
  }

  @Test
  public void bind_insertLandi_ListViewSelectedLandisConatinsLandi() {
    new JFXPanel();
    Platform.runLater(new Runnable() {

      @Override
      public void run() {
        PRODSQLRUN prodsqlrun = new PRODSQLRUN();
        ArrayList<Landi> listToAdd = new ArrayList<Landi>();
        Landi landiToTest = new Landi("Test", "", "", "");
        listToAdd.add(landiToTest);
        ObservableList<Landi> landiList = FXCollections.observableArrayList(listToAdd);
        prodsqlrun.addLandisToSelectedLandiList(landiList);
        assertThat(prodsqlrun.getlistViewSelectedLandiContent(), hasItem(landiToTest));
      }
    });
  }

  @Test
  public void clear_clearLists_getEmptyListViews() {
    new JFXPanel();
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        PRODSQLRUN prodsqlrun = new PRODSQLRUN();
        ArrayList<Landi> listToAdd = new ArrayList<Landi>();
        Landi landiToTest = new Landi("Test", "", "", "");
        listToAdd.add(landiToTest);
        ObservableList<Landi> landiList = FXCollections.observableArrayList(listToAdd);
        prodsqlrun.addLandisToSelectedLandiList(landiList);
        prodsqlrun.clearLandiLists();
        assertTrue(prodsqlrun.getlistViewSelectedLandiContent().isEmpty());
        assertTrue(prodsqlrun.getlistViewDefaultLandiContent().isEmpty());
      }
    });
  }
}


package prodsqlrun;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class MessagesTest {

  @Test
  public void getMessage() {
    assertThat(Messages.getString("info"), is("Info"));
  }

  @Test
  public void getInvalidMessageGetEmpty() {
    assertThat(Messages.getString("invalidString"), is(""));
  }

}
